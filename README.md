# klarna
web service which calculates and displays results for the following
algorithms:

- Calculating a Fibonacci number F(n) with the value of n provided by the user.
- The Ackermann function A(m,n) with values of m and n provided by the user.
- The factorial of a non-negative integer n provided by the user.

### Installation
* ######pre-requirements:
  * python 3.5+
  * django
  * django-framework
  
* `git clone https://bitbucket.org/mmaroufomar/klarna.git`

### Running the service
`python manage.py runserver`

### Running the test cases
`python manage.py test`

### Test links
* Factorial:
    
    - http://127.0.0.1:8000/factorial/9/
* Fibonacci: 
    
    - http://127.0.0.1:8000/fibonacci/9/
    - http://127.0.0.1:8000/fibonacci/-9/
* Ackermann: 
    
    - http://127.0.0.1:8000/ackermann/9/


##### time lines that took to solve each function
1- Factorial:
    
    * I heared about it from my school in the past
    * I didn't find any problems to solve and imlement the function
    * It took around 10 minuits for the function and 10 min for test cases 

2- Fibonacci

    * This is the first time i heards about it
    * I read its wiki and i understod it
    * It took around 1 hour reading and 30 minuits for the function and 15 min for test cases 
    
3- Ackermann
    
    * The hardest part for it is to understand the equation
    * I set with my wife to understand it as she is Math teacher
    * I start the Implementation and rewrite it more than 3 times, the main issue was understanding it. after i understand what should i do i solved it and Implement the right methods with its test cases
    * It took around  3 days to start understanding it
    * The Implementation took around 2 hours and the test cases 15 min