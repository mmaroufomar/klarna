from unittest import mock
from unittest.mock import Mock, create_autospec

from django.test import TestCase

from calculator.main.calculatorContext import CalculatorStrategy, CalculatorContext


class CalculatorContextTest(TestCase):

    @mock.patch('calculator.main.calculatorStrategies.FactorialStrategy',
                **{'return_value.raiseError.side_effect': Exception()})
    def test_validationFailed(self, MockStrategy):
        MockStrategy.validate.raiseError.side_effect = Mock(side_effect=Exception('Test'))
        calc = CalculatorContext(MockStrategy)
        calc.calculate(4, 3)

    @mock.patch('calculator.main.calculatorStrategies.FactorialStrategy')
    def test_calculateWithRequiredPass(self, MockStrategy):
        MockStrategy.calculate.return_value = 123
        MockStrategy.is_reuired.return_value = True
        calc = CalculatorContext(MockStrategy)
        calc_val = calc.calculate(4, 3)
        self.assertEqual(calc_val, 123)


    @mock.patch('calculator.main.calculatorStrategies.FactorialStrategy')
    def test_calculatePass(self, MockStrategy):
        MockStrategy.calculate.return_value = 123
        calc = CalculatorContext(MockStrategy)
        calc_val = calc.calculate(4, 3)
        self.assertEqual(calc_val, 123)


    @mock.patch('calculator.main.calculatorStrategies.FactorialStrategy')
    def test_calculateFailed(self, MockStrategy):
        MockStrategy.calculate.return_value = 123
        calc = CalculatorContext(MockStrategy)
        calc_val = calc.calculate(4, 3)
        self.assertNotEqual(calc_val, 1236)


    @mock.patch('calculator.main.calculatorStrategies.FactorialStrategy')
    def test_calculateFailed(self, MockStrategy):
        MockStrategy.calculate.return_value = 123
        calc = CalculatorContext(MockStrategy)
        calc_val = calc.calculate(4, 3)
        self.assertNotEqual(calc_val, 1236)