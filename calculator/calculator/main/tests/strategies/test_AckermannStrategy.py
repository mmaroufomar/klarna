from unittest import mock
from unittest.mock import Mock, create_autospec

from django.test import TestCase

from calculator.main.calculatorContext import CalculatorStrategy, CalculatorContext
from calculator.main.calculatorStrategies import FactorialStrategy, AckermannStrategy


class AckermannStrategyTest(TestCase):

    def test_valueMLessThanTwo(self):
        ackermann = AckermannStrategy()
        value = ackermann.calculate(1, 4)
        self.assertEqual(value, 6)

    def test_valueMIsThree(self):
        ackermann = AckermannStrategy()
        value = ackermann.calculate(3,4)
        self.assertEqual(value, 125)

    def test_valueMAboveThree(self):
        ackermann = AckermannStrategy()
        value = ackermann.calculate(4, 1)
        self.assertEqual(value, 65533)
