from unittest import mock
from unittest.mock import Mock, create_autospec

from django.test import TestCase

from calculator.main.calculatorContext import CalculatorStrategy, CalculatorContext
from calculator.main.calculatorStrategies import FactorialStrategy


class FactorialStrategyTest(TestCase):

    def test_valueIsZero(self):
        factorial = FactorialStrategy()
        value = factorial.calculate(0)
        self.assertEqual(value, 1)

    def test_valueIsOne(self):
        factorial = FactorialStrategy()
        value = factorial.calculate(0)
        self.assertEqual(value, 1)

    def test_valueAboveOne(self):
        factorial = FactorialStrategy()
        value = factorial.calculate(5)
        self.assertEqual(value, 120)