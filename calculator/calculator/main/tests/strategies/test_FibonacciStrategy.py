from unittest import mock
from unittest.mock import Mock, create_autospec

from django.test import TestCase

from calculator.main.calculatorContext import CalculatorStrategy, CalculatorContext
from calculator.main.calculatorStrategies import FactorialStrategy, FibonacciStrategy


class FibonacciStrategyTest(TestCase):

    def test_valueIsZero(self):
        factorial = FactorialStrategy()
        value = factorial.calculate(0)
        self.assertEqual(value, 1)

    def test_valueIsOne(self):
        factorial = FactorialStrategy()
        value = factorial.calculate(0)
        self.assertEqual(value, 1)

    def test_valueAboveOne(self):
        fibonacci = FibonacciStrategy()
        value = fibonacci.calculate(8)
        self.assertEqual(value, 21)

    def test_valueIsNegative(self):
        fibonacci = FibonacciStrategy()
        value = fibonacci.calculate(-8)
        self.assertEqual(value, -21)