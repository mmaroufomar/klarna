from abc import abstractmethod

from django.views.generic.base import logger


class CalculatorStrategy(object):
    is_m_required = False

    @abstractmethod
    def calculate(self, n: int, m: int = 1) -> int:
        pass

    def validate(self, number: int) -> None:
        if number < 0:
            raise Exception("Numbers must be not negative")


class CalculatorContext:
    def __init__(self, strategy: CalculatorStrategy) -> None:
        self._strategy = strategy

    @property
    def strategy(self) -> CalculatorStrategy:
        return self._strategy

    @strategy.setter
    def strategy(self, strategy: CalculatorStrategy):
        self.strategy = strategy

    def calculate(self, n: int, m: int = -1) -> int:
        try:
            self._strategy.validate(n)
            if self._strategy.is_m_required:
                self._strategy.validate(m)
            result = self._strategy.calculate(n, m)
        except Exception as e:
            logger.warn(str(e))
            raise e
        return result
