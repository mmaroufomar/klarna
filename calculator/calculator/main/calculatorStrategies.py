from calculator.main.calculatorContext import CalculatorStrategy, CalculatorContext


class FactorialStrategy(CalculatorStrategy):

    def calculate(self, n, m=-1):
        if n == 0 or n == 1:
            return 1
        elif n > 1:
            return n * self.calculate(n - 1)


class FibonacciStrategy(CalculatorStrategy):

    def validate(self, number):
        """
        Fibonacci number allowed negative numbers
        https://en.wikipedia.org/wiki/Fibonacci_number#Sequence_properties
        """
        pass

    def calculate(self, n: int, m=-1):
        if n == 0 or n == 1:
            return n
        if n < 0:
            abs_n = abs(n)
            return ((-1) ** (abs_n + 1)) * self.calculate(abs_n)
        return self.calculate(n - 1) + self.calculate(n - 2)


class AckermannStrategy(CalculatorStrategy):
    is_m_required = True
    calc_context = CalculatorContext(FactorialStrategy())

    def calculate(self, m, n=-1):
        if m <= 2:
            ackermann_number = (self.calc_context.calculate(m) * n) + (m + 1)
        elif m == 3:
            power = n + 3
            ackermann_number = (2 ** power) - 3
        else:
            power = m - 2
            counter = n + 3
            ackermann_number = (self.getPowers(2**power, counter)) - 3
        return ackermann_number

    def getPowers(self, number, counter):
        if counter > 1:
            return self.getPowers(number * number, counter - 1)
        return number
