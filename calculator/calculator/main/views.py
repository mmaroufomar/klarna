from django.views.generic.base import logger
from rest_framework.status import HTTP_400_BAD_REQUEST
from rest_framework.views import APIView
from rest_framework.response import Response

from calculator.main.calculatorContext import CalculatorContext
from calculator.main.calculatorStrategies import FactorialStrategy, FibonacciStrategy, AckermannStrategy


class FactorialView(APIView):
    def get(self, request, n: int):
        logger.debug("Getting " + str(n))
        fact = CalculatorContext(FactorialStrategy())
        factorial = fact.calculate(n)
        content = {'Factorial': factorial}
        return Response(content)


class FibonacciView(APIView):
    def get(self, request, n):
        try:
            logger.debug("Getting " + str(n))
            fib = CalculatorContext(FibonacciStrategy())
            fibonacci = fib.calculate(n)
        except Exception as e:
            logger.warn(str(e))
            return Response(status=HTTP_400_BAD_REQUEST)
        content = {'Fibonacci': fibonacci}
        return Response(content)


class AckermannView(APIView):
    def get(self, request, m, n):
        try:
            logger.debug("Getting " + str(m) + "," + str(n))
            ack = CalculatorContext(AckermannStrategy())
            ackermann = ack.calculate(m, n)
        except Exception as e:
            logger.warn(str(e))
            return Response(status=HTTP_400_BAD_REQUEST)
        content = {'Ackermann': ackermann}
        return Response(content)
